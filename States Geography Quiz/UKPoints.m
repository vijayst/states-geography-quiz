//
//  UKPoints.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 09/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

// difficulty - 23, 24
const int kCapitalArray[3][4] = {{ 9, 5, 2, 1 },{ 12, 6, 3, 1 },{ 15, 8, 4, 1 }};
const int kFlagArray[3][4] = {{ 25, 12, 6, 2 },{ 40, 20, 10, 2 },{ 30, 16, 8, 2 }};
const int kMapArray[3][4] = {{ 25, 12, 6, 2 },{ 40, 20, 10, 2 },{ 30, 16, 8, 2 }};
const int kBonusPoints = 82;
const int kCapitalBonusPoints = 82;
const int kFlagBonusPoints = 42;
const int kMapBonusPoints = 42;
const int kMaxPoints = 918;
const int kTime1Points = 9;
const int kTime2Points = 5;
const int kTime3Points = 2;
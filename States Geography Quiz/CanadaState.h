//
//  CAState.h
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 10/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "State.h"

@interface CanadaState : State
@end

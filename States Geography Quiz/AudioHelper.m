//
//  AudioHelper.m
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "AudioHelper.h"

@interface AudioHelper()
{
    enum AudioType audioType;
}
-(void)play:(NSURL *)url;
@end

@implementation AudioHelper
@synthesize player;

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
    
    }
    return self;
}

+(void)createAudioSession
{
    NSError *error;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    BOOL success = [[AVAudioSession sharedInstance] setActive:YES error: &error];
    if(!success)
        NSLog(@"The audio session activation error is %@" , error.description);
    else
        NSLog(@"The audio session is activated");
}

-(void)playAppLoadSound
{
    audioType = AppLoad;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"appload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playGameLoadSound
{
    audioType = GameLoad;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gameload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playMatchSound
{
    audioType = Match;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"match" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playNoMatchSound
{
    audioType = NoMatch;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"nomatch" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)play:(NSURL *)url
{
    NSError *error;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
    if(error)
        NSLog(@"Error in loading - %@", error.description);
    player.volume = 0.4;
    player.delegate = self;
    [player play];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.delegate audioCompleted:audioType];
}


@end

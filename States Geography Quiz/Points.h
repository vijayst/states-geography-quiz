//
//  Points.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

const int kCapitalArray[3][4];
const int kFlagArray[3][4];
const int kMapArray[3][4];
const int kBonusPoints;
const int kCapitalBonusPoints;
const int kFlagBonusPoints;
const int kMapBonusPoints;
const int kMaxPoints;
const int kTime1Points;
const int kTime2Points;
const int kTime3Points;

@interface Points : NSObject
+(int)calculate:(enum QuizType)type attempt:(int)attempt time:(int)time difficulty:(int)difficulty;
+(int)getMaximumPoints:(enum GameType)gameType quizType:(enum QuizType)quizType;
+(int)getBonusPoints:(enum GameType)gameType quizType:(enum QuizType)quizType;
@end

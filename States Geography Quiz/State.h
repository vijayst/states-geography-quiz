//
//  Country.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface State : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *capital;
@property (strong, nonatomic) NSString *iso;
@property (strong, nonatomic) NSString *flag;
@property (strong, nonatomic) NSString *map;
@property (assign, nonatomic) int difficulty;
+(NSArray *)getStates;
@end

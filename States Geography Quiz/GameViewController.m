//
//  GameViewController.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 07/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "GameViewController.h"
#import "ViewController.h"
#import "Translation.h"
#import "AudioHelper.h"
#import "Points.h"
#import <iAd/iAd.h>
@interface GameViewController ()

@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *questionIcon;
@property (strong, nonatomic) IBOutlet UILabel *questionInnerLabel;
@property (strong, nonatomic) IBOutlet UIButton *choice1Button;
@property (strong, nonatomic) IBOutlet UIButton *choice2Button;
@property (strong, nonatomic) IBOutlet UIButton *choice3Button;
@property (strong, nonatomic) IBOutlet UIButton *choice4Button;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UILabel *maxLabel;
@property (strong, nonatomic) IBOutlet UILabel *levelLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *questionheightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *questioninnerheightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *choice1widthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *choice1heightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *qcspacingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *maxwidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *maxheightConstraint;

- (IBAction)showMenu:(id)sender;
- (IBAction)tapAnswer:(id)sender;

@end

@implementation GameViewController
{
    // constant but not a compile time constant.
    UIColor *_matchColor;
    UIColor *_noMatchColor;
    UIColor *_panelColor;
    UIColor *_timeoutColor;
    
    NSString *_timeText;
    NSString *_scoreText;
    NSString *_levelText;
    NSString *_maxText;
    
    UIImage *_capitalImage;
    UIImage *_flagImage;
    UIImage *_mapImage;
    NSString *_flagDirectory;
    NSString *_mapDirectory;
    
    NSArray *_buttons;
    BOOL _drawImageInChoice;
    BOOL _drawImageInQuestion;
    
    BOOL _iPhone;
    float _iPhoneHeaderFont;
    float _iPadHeaderFont;
    float _headerFontSize;
    
    BOOL _animComplete;
    BOOL _startNewLevel;
    int _startNewLevelTime;
    BOOL _initImage;
    
    Translation *_translation;
    AudioHelper *_audioHelper;
    AnimationHelper *_animHelper;
    TTSHelper *_ttsHelper;
    NSTimer *_timer;
}

static float kImageMargin = 10.0F;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ADBannerView *adView = [[ADBannerView alloc] initWithFrame:CGRectMake(0,20, 320, 50)];
    [self.view addSubview:adView];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    _iPhone = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone;
    if(_iPhone)
    {
        // update constraints based on size of iPhone.
        [self updateConstraints];
    }

    [self initHelpers];
    [self initLabels];
    [self initButtons];
    
    _initImage = NO;
    self.view.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.game.paused)
        [self getDataForNewLevel];
    else {
        // refresh the view based on the game state.
        [self refreshView];
        [self updateButtons];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Start the timer only when started from a paused state.
    if(self.game.paused)
        [self createTimer];
    else
    {
        self.view.hidden = NO;
        self.view.userInteractionEnabled = NO;
        self.homeButton.enabled = NO;
        [self animate];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UI initialization methods
-(BOOL)getDataForNewLevel
{
    BOOL result = [self.game getQuiz];
    if(!result) {
        ViewController *controller = (ViewController *)self.presentingViewController;
        controller.silent = YES;
        [self dismissViewControllerAnimated:NO completion:^{
            [controller showGameOver];
        }];
        
        return NO;
    }
    
    [self refreshView];
    return YES;
}

-(void)refreshView
{
    _drawImageInQuestion = ((self.game.quiz.type==Flags || self.game.quiz.type==Maps) && self.game.quiz.reverse);
    if(_drawImageInQuestion)
    {
        [self updateLabel:self.questionInnerLabel
            withImageName:self.game.quiz.question
              forQuizType:self.game.quiz.type];
    }
    else
    {
        self.questionInnerLabel.text =  self.game.quiz.question;
        self.questionInnerLabel.backgroundColor = [UIColor clearColor];
    }
    
    [self setImage:self.game.quiz.type];
    for(UIButton *button in _buttons)
    {
        [button setTitle:@"" forState:UIControlStateNormal];
    }
    
    if(!_initImage)
        [self initImages];
    
    UIColor *labelColor;
    switch(self.game.quiz.type)
    {
        case Capitals:
            labelColor = [UIColor colorWithPatternImage:_capitalImage];
            break;
        case Flags:
            labelColor = [UIColor colorWithPatternImage:_flagImage];
            break;
        case Maps:
            labelColor = [UIColor colorWithPatternImage:_mapImage];
            break;
        case QuizTypeNone:
            break;
    }
    
    _drawImageInChoice = ((self.game.quiz.type==Flags || self.game.quiz.type==Maps) && !self.game.quiz.reverse);
    
    self.choice1Button.backgroundColor = labelColor;
    self.choice2Button.backgroundColor = labelColor;
    self.choice3Button.backgroundColor = labelColor;
    self.choice4Button.backgroundColor = labelColor;
    
    labelColor = self.game.quiz.time > 0 ? _panelColor : _timeoutColor;
    self.levelLabel.backgroundColor = labelColor;
    self.timeLabel.backgroundColor = labelColor;
    self.scoreLabel.backgroundColor = labelColor;
    self.maxLabel.backgroundColor = labelColor;
    self.homeButton.backgroundColor = labelColor;
    
    _headerFontSize = _iPhone ? _iPhoneHeaderFont : _iPadHeaderFont;
    self.levelLabel.attributedText = [self getLevelLabel];
    self.timeLabel.attributedText = [self getTimerLabel];
    self.scoreLabel.attributedText = [self getScoreLabel];
    self.maxLabel.attributedText = [self getMaxLabel];
}

-(void)animate
{
    [_audioHelper playGameLoadSound];
    _animComplete = NO;
    self.choice1Button.enabled
    = self.choice2Button.enabled
    = self.choice3Button.enabled
    = self.choice4Button.enabled
    = YES;
    
    [_animHelper zoom:self.questionLabel];
    [_animHelper zoom:self.questionInnerLabel];
    [_animHelper zoom:self.questionIcon];
    [_animHelper fade:self.questionIcon];
    [_animHelper fade:self.choice1Button];
    [_animHelper fade:self.choice2Button];
    [_animHelper fade:self.choice3Button];
    [_animHelper fade:self.choice4Button];
}

-(void)newLevel
{
    BOOL result = [self getDataForNewLevel];
    if(result) {
        [self animate];
    }
}

#pragma mark AnimationCompletedDelegate protocol
-(void)animationCompleted
{
    if(!_animComplete) {
        _animComplete = YES;
        [_ttsHelper speakText:self.game.quiz.prompt];
    }
}

#pragma mark SpeechCompletedDelegate protocol
-(void)speechCompleted
{
    [self updateButtons];
    self.view.userInteractionEnabled = true;
    self.homeButton.enabled = true;
    // start timer
    _startNewLevel = NO;
    _startNewLevelTime = 0;
    [self performSelectorOnMainThread:@selector(createTimer)
                           withObject:nil
                        waitUntilDone:YES];
}

#pragma mark Choice Button methods
-(void)updateButtons
{
    self.choice1Button.enabled
    = self.choice2Button.enabled
    = self.choice3Button.enabled
    = self.choice4Button.enabled
    = YES;
    
    if(!_drawImageInChoice)
    {
        [self.choice1Button setTitle:self.game.quiz.choice1 forState:UIControlStateNormal];
        [self.choice2Button setTitle:self.game.quiz.choice2 forState:UIControlStateNormal];
        [self.choice3Button setTitle:self.game.quiz.choice3 forState:UIControlStateNormal];
        [self.choice4Button setTitle:self.game.quiz.choice4 forState:UIControlStateNormal];
    }
    else
    {
        [self updateButton:self.choice1Button
                 imageName:self.game.quiz.choice1
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice2Button
                 imageName:self.game.quiz.choice2
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice3Button
                 imageName:self.game.quiz.choice3
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice4Button
                 imageName:self.game.quiz.choice4
                  drawType:0
                  gameType:self.game.quiz.type];
    }
    
    NSArray *buttonArray = @[self.choice1Button, self.choice2Button, self.choice3Button, self.choice4Button];
    for(int i=0; i < 4; i++)
    {
        BOOL enabled = [self.game.quiz.enabledArray[i] boolValue];
        if(!enabled) {
            UIButton *button = (UIButton *)buttonArray[i];
            button.enabled = NO;
            if(_drawImageInChoice)
            {
                [self updateButton:button
                          drawType:2
                          gameType:self.game.quiz.type];
            }
            else
            {
                button.backgroundColor = _noMatchColor;
            }
        }
    }
}

-(void)updateButton:(UIButton *)button drawType:(int)drawType gameType:(enum QuizType)gameType
{
    switch(button.tag)
    {
        case 0:
            [self updateButton:button
                     imageName:self.game.quiz.choice1
                      drawType:drawType
                      gameType:gameType];
            break;
        case 1:
            [self updateButton:button
                     imageName:self.game.quiz.choice2
                      drawType:drawType
                      gameType:gameType];
            break;
        case 2:
            [self updateButton:button
                     imageName:self.game.quiz.choice3
                      drawType:drawType
                      gameType:gameType];
            break;
        case 3:
            [self updateButton:button
                     imageName:self.game.quiz.choice4
                      drawType:drawType
                      gameType:gameType];
            break;
    }
}

-(void)updateButton:(UIButton *)button imageName:(NSString *)imageName drawType:(int)drawType gameType:(enum QuizType)gameType
{
    NSString *path = nil;
    if(gameType==Flags)
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:_flagDirectory];
    }
    else
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:_mapDirectory];
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    float scale = image.size.width / image.size.height;
    CGSize size = button.frame.size;
    float labelScale = (size.width - 2*kImageMargin) / (size.height - 2*kImageMargin);
    UIGraphicsBeginImageContext( size );
    
    // draw the original flag background
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0,0,size.width, size.height);
    if(drawType==0)
    {
        if(gameType==Flags)
            [_flagImage drawInRect:rect];
        else
            [_mapImage drawInRect:rect];
    }
    else if(drawType==1)
    {
        CGContextSetFillColorWithColor(context, [_matchColor CGColor]);
        CGContextFillRect(context, rect);
    }
    else if(drawType==2)
    {
        CGContextSetFillColorWithColor(context, [_noMatchColor CGColor]);
        CGContextFillRect(context, rect);
    }
    
    // scale the flag and draw within a narrow boundary
    if(scale > labelScale)
    {
        float newHeight = (size.width - 2*kImageMargin) / scale;
        float newOrigin = (size.height - newHeight)/2;
        [image drawInRect:CGRectMake(kImageMargin,newOrigin,size.width - 2*kImageMargin,newHeight)];
    }
    else
    {
        float newWidth = (size.height - 2*kImageMargin)* scale;
        float newOrigin = (size.width-newWidth)/2;
        [image drawInRect:CGRectMake(newOrigin,kImageMargin,newWidth,size.height-2*kImageMargin)];
    }
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    button.backgroundColor = [UIColor colorWithPatternImage:newImage];
}

-(void)updateLabel:(UILabel *)label withImageName:(NSString *)imageName forQuizType:(enum QuizType)quizType
{
    NSString *path = nil;
    if(quizType==Flags)
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:_flagDirectory];
    }
    else
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:_mapDirectory];
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    float scale = image.size.width / image.size.height;
    CGSize size = label.frame.size;
    float labelScale = size.width / size.height;
    
    UIGraphicsBeginImageContext( size );
    
    // scale the flag and draw within a narrow boundary
    if(scale > labelScale)
    {
        float newHeight = size.width / scale;
        float newOrigin = (size.height - newHeight)/2;
        [image drawInRect:CGRectMake(0,newOrigin,size.width,newHeight)];
    }
    else
    {
        float newWidth = size.height * scale;
        float newOrigin = (size.width-newWidth)/2;
        [image drawInRect:CGRectMake(newOrigin,0,newWidth,size.height)];
    }
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    label.text = @"";
    label.backgroundColor = [UIColor colorWithPatternImage:newImage];
}

-(UIImage *)resize:(UIImage *)image
{
    float width = self.choice1Button.bounds.size.width;
    float height = self.choice1Button.bounds.size.height;
    UIGraphicsBeginImageContextWithOptions(self.choice1Button.bounds.size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)setImage:(enum QuizType)type
{
    switch(type)
    {
        case Capitals:
            self.questionIcon.image = [UIImage imageNamed:@"capitals_icon.png"];
            break;
        case Flags:
            self.questionIcon.image = [UIImage imageNamed:@"flags_icon.png"];
            break;
        case Maps:
            self.questionIcon.image = [UIImage imageNamed:@"maps_icon.png"];
            break;
        case QuizTypeNone:
            break;
    }
}

#pragma mark Initialization methods
-(void)updateConstraints
{
    CGFloat h = self.view.bounds.size.height;
    CGFloat qheight = 150.0F;
    CGFloat cwidth = 130.0F;
    CGFloat cheight = 120.0F;
    CGFloat qcspacing = 29.0F;
    CGFloat pwidth = 52.0F;
    CGFloat pheight = 60.0F;
    
    if(h==480)
    {
        qcspacing = 20.0F;
        qheight = 120.0F;
        cheight = 100.0F;
    }
    if(h==667)
    {
        qheight *= 1.17;
        qcspacing *= 1.17;
        cwidth = 157.5;
        cheight = 144.0;
        pwidth *= 1.17;
        pheight *= 1.17;
    }
    if(h==736)
    {
        qheight *= 1.29;
        qcspacing = 40.0;
        cwidth = 177;
        cheight = 160;
        pwidth *= 1.29;
        pheight *= 1.29;
    }
    
    self.questionheightConstraint.constant = qheight;
    self.questioninnerheightConstraint.constant = qheight - 20.0F;
    self.qcspacingConstraint.constant = qcspacing;
    self.choice1widthConstraint.constant = cwidth;
    self.choice1heightConstraint.constant = cheight;
    self.maxwidthConstraint.constant = pwidth;
    self.maxheightConstraint.constant = pheight;
}

-(void)initHelpers
{
    _audioHelper = [[AudioHelper alloc] init];
    _ttsHelper = [[TTSHelper alloc] init];
    _ttsHelper.delegate = self;
    _animHelper = [[AnimationHelper alloc] init];
    _animHelper.delegate = self;
}

-(void)initLabels
{
    _translation = [[Translation alloc] init];
    _timeText = [_translation getTranslation:@"Time"];
    _levelText = [_translation getTranslation:@"Level"];
    _maxText = [_translation getTranslation:@"Max"];
    _scoreText = [_translation getTranslation:@"Score"];
    
    static NSString *iPhoneFontKey = @"iPhone Game Label Header Font";
    static NSString *iPadFontKey = @"iPad Game Label Header Font";
    _iPhoneHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPhoneFontKey] floatValue];
    _iPadHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPadFontKey] floatValue];
    
    _panelColor = [UIColor colorWithRed:0
                                  green:33/256.0
                                   blue:81/256.0
                                  alpha:1];
    
    _timeoutColor = [UIColor colorWithRed:187/256.0
                                    green:21/256.0
                                     blue:27/256.0
                                    alpha:1];
    
    self.timeLabel.textAlignment =
    self.maxLabel.textAlignment =
    self.scoreLabel.textAlignment =
    self.levelLabel.textAlignment =
    NSTextAlignmentCenter;
    
    self.timeLabel.textColor =
    self.maxLabel.textColor =
    self.scoreLabel.textColor =
    self.levelLabel.textColor =
    [UIColor whiteColor];
}

-(void)initImages
{
    _capitalImage = [self resize:[UIImage imageNamed:@"capitals.png"]];
    _flagImage = [self resize:[UIImage imageNamed:@"flags.png"]];
    _mapImage = [self resize:[UIImage imageNamed:@"maps.png"]];
    
    NSString *dirName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Directory name"];
    _flagDirectory = [NSString stringWithFormat:@"%@/flags", dirName];
    _mapDirectory = [NSString stringWithFormat:@"%@/maps", dirName];
    
    _initImage = YES;
}

-(void)initButtons
{
    _buttons = @[self.choice1Button, self.choice2Button, self.choice3Button, self.choice4Button];
    for(UIButton *button in _buttons)
    {
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    _matchColor = [UIColor colorWithRed:0
                                  green:0.6
                                   blue:0
                                  alpha:0.8];
    _noMatchColor = [UIColor colorWithRed:1
                                    green:0
                                     blue:0
                                    alpha:0.8];

}

#pragma mark Timer methods
-(void)createTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1
                                              target:self
                                            selector:@selector(updateTime)
                                            userInfo:Nil
                                             repeats:true];
}

-(void)stopTimer
{
    [_timer invalidate];
    _timer = nil;
}

-(void)pauseGame
{
    [self stopTimer];
    self.game.paused = YES;
}

-(void)resumeGame
{
    if(self.game.paused)
    {
        [self createTimer];
        self.game.paused = NO;
    }
}

-(void)updateTime
{
    if(_startNewLevel) {
        if(++_startNewLevelTime==3) {
            [self stopTimer];
            [self newLevel];
        }
    } else {
        if(self.game.quiz.time>0) {
            self.game.quiz.time--;
            self.timeLabel.attributedText = [self getTimerLabel];
            if(self.game.quiz.time==0)
            {
                self.levelLabel.backgroundColor = _timeoutColor;
                self.timeLabel.backgroundColor = _timeoutColor;
                self.scoreLabel.backgroundColor = _timeoutColor;
                self.maxLabel.backgroundColor = _timeoutColor;
                self.homeButton.backgroundColor = _timeoutColor;
            }
        }
    }
}

#pragma mark Set panel label text
-(NSAttributedString *)getLevelLabel
{
    NSInteger len = [_levelText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc]
                                            initWithString:[NSString stringWithFormat:@"%@\n \n%d",
                                                            _levelText,
                                                            self.game.level]];
    [labelText addAttribute:NSFontAttributeName
                      value:[UIFont boldSystemFontOfSize:_headerFontSize]
                      range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName
                      value:[UIFont boldSystemFontOfSize:5.0F]
                      range:NSMakeRange(len+1,1)];
    [labelText addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:NSMakeRange(0, [labelText length])];
    return labelText;
}

-(NSAttributedString *)getTimerLabel
{
    NSInteger len = [_timeText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", _timeText, self.game.quiz.time]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getScoreLabel
{
    NSInteger len = [_scoreText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", _scoreText, self.game.score]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getMaxLabel
{
    NSInteger len = [_maxText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", _maxText, self.game.maxPoints]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

// TODO: showing the menu is not smooth.
- (IBAction)showMenu:(id)sender {
    [self pauseGame];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)tapAnswer:(id)sender
{
    if(_startNewLevel)
        return;
    
    UIButton *button = (UIButton *)sender;
    if(button!=nil) {
        if(button.tag==self.game.quiz.answer) {
            self.view.userInteractionEnabled = false;
            self.homeButton.enabled = false;
            self.game.quiz.attempts++;
            if(!_drawImageInChoice)
            {
                button.backgroundColor =  _matchColor;
            }
            else
            {
                [self updateButton:button
                          drawType:1
                          gameType:self.game.quiz.type];
            }
            
            [_audioHelper playMatchSound];
            int points =    [Points calculate:self.game.quiz.type
                                      attempt:self.game.quiz.attempts
                                         time:self.game.quiz.time
                                   difficulty:self.game.quiz.difficulty];
            self.game.score += points;
            self.scoreLabel.attributedText = [self getScoreLabel];
            _startNewLevel = true;
        } else {
            if([self.game.quiz.enabledArray[button.tag] boolValue]) {
                if(!_drawImageInChoice)
                {
                    button.backgroundColor = _noMatchColor;
                }
                else
                {
                    [self updateButton:button drawType:2
                              gameType:self.game.quiz.type];
                }
                self.game.quiz.attempts++;
                [_audioHelper playNoMatchSound];
                self.game.quiz.enabledArray[button.tag] = @NO;
            }
        }
    }
}

@end

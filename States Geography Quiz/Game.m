//
//  Game.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Game.h"
#import "State.h"
#import "Points.h"
#import "Translation.h"

@interface Game()
{
    enum GameType _gameType;
    enum QuizType _quizType;
    int _answerPosition; // 0 - 3
    int _question; // array position
    int _count;
}
@property (strong, nonatomic) NSMutableArray *gameArray;
@property (strong, nonatomic) NSMutableArray *typeArray;
@property (nonatomic, strong) Translation *translation;

-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType;

@end

@implementation Game

@synthesize typeArray;
@synthesize gameArray;
@synthesize score;
@synthesize  level;


+(Game *)createGame:(enum GameType)gameType andType:(enum QuizType)quizType
{
    Game* newGame = [[Game alloc] init];
    if(newGame) {
        [newGame setGameType:gameType andQuizType:quizType];
    }
    return newGame;
}

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.translation = [[Translation alloc] init];
        self.level = 0;
        self.score = 0;
        NSString *className = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Class name"];
        Class stateClass = NSClassFromString(className);
        self.states = [stateClass getStates];
        _count = (int)[self.states count];
        
        NSMutableArray *capitalArray = [[NSMutableArray alloc] initWithCapacity:_count];
        NSMutableArray *flagArray = [[NSMutableArray alloc] initWithCapacity:_count];
        NSMutableArray *mapArray = [[NSMutableArray alloc] initWithCapacity:_count];
        for (int i=0; i<_count; i++)
        {
            State *state = self.states[i];
            if(![state.capital isEqualToString:@""])
                [capitalArray addObject:[NSNumber numberWithInt:i]];
            if(![state.flag isEqualToString:@""])
               [flagArray addObject:[NSNumber numberWithInt:i]];
            [mapArray addObject:[NSNumber numberWithInt:i]];
        }
        self.gameArray = [NSMutableArray arrayWithArray:@[capitalArray, flagArray, mapArray]];
    }
    return self;
}


-(enum GameType)gameType
{
    return _gameType;
}

-(int)maxPoints
{
    return [Points getMaximumPoints:_gameType quizType:_quizType];
}

-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType
{
    _gameType = gameType;
    _quizType = quizType;
    
    switch(gameType)
    {
        case GameTypeSingle:
            switch(quizType)
            {
                case Capitals:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals]]];
                    break;
                case Flags:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Flags]]];
                    break;
                case Maps:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Maps]]];
                case QuizTypeNone:
                    break;
            }
            break;
        case GameTypeMulti:
            typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals],
                                                         [NSNumber numberWithInt:Flags],
                                                         [NSNumber numberWithInt:Maps]]];
            break;
    }
}


-(BOOL)getQuiz
{
    // no more quizzes
    if(self.over)
        return NO;
    
    level++;
    int questionType = _quizType;
    if(_gameType == GameTypeMulti) {
        int questionTypeIndex = arc4random() % [typeArray count];
        questionType = [self.typeArray[questionTypeIndex] intValue];
    }
    
    NSMutableArray *quizItems = [gameArray objectAtIndex:questionType];
    int questionNumberIndex = arc4random() % [quizItems count];
    NSNumber *questionNumber = [quizItems objectAtIndex:questionNumberIndex];
    [quizItems removeObjectAtIndex:questionNumberIndex];
    // if last element in the sequence, that sequence is complete.
    if([quizItems count]==0) {
        if(_gameType==GameTypeMulti) {
            [gameArray removeObjectAtIndex:questionType];
            [typeArray removeObjectAtIndex:questionType];
            if([typeArray count]==0) {
                self.over = YES;
            }
        }
        else {
            self.over = YES;
        }
    }
    
    _question = [questionNumber intValue];
    State *state = [self.states objectAtIndex:_question];
    BOOL reverse = _question % 3 == 0;
    
    self.quiz = [[Quiz alloc] init];
    NSMutableArray *answerArray;
    NSString *prompt;
    
    // question type is different from game type.
    // game type can be full
    // question type is 0 - 3.
    if(reverse)
    {
        NSString *entityName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Entity name"];
        switch(questionType)
        {
            case Capitals:
            {
                answerArray = [self getStates:state.capital
                                      withAnswer:state.name
                                     forQuizType:questionType];
                NSString *promptText = [NSString stringWithFormat:@"What is the %@ with the capital of ", entityName];
                promptText = [promptText stringByAppendingString:@"%@?"];
                prompt = [self.translation getTranslation:promptText];
                self.quiz.question = state.capital;
                prompt = [NSString stringWithFormat:prompt, state.capital];
                break;
            }
            case Flags:
            {
                answerArray = [self getStates:state.flag
                                      withAnswer:state.name
                                     forQuizType:questionType];
                NSString *promptText = [NSString stringWithFormat:@"What is the %@ whose flag is shown?", entityName];
                prompt = [self.translation getTranslation:promptText];
                self.quiz.question = state.flag;
                break;
            }
            case Maps:
            {
                answerArray = [self getStates:state.map
                                      withAnswer:state.name
                                     forQuizType:questionType];
                NSString *promptText = [NSString stringWithFormat:@"What is the %@ whose map is shown?", entityName];
                prompt = [self.translation getTranslation:promptText];
                self.quiz.question = state.map;
                break;
            }
            default:
                break;
        }
    }
    else {
        switch(questionType)
        {
            case Capitals:
                answerArray = [self getCapitals:state.capital];
                prompt = [self.translation getTranslation:@"What is the capital of %@?"];
                break;
            case Flags:
                answerArray = [self getFlags:state.flag];
                prompt = [self.translation getTranslation:@"Identify the flag of %@?"];
                break;
            case Maps:
                answerArray = [self getMaps:state.map];
                prompt = [self.translation getTranslation:@"Identify the map of %@?"];
                break;
            default:
                break;
        }
        
        self.quiz.question = state.name;
        prompt = [NSString stringWithFormat:prompt, state.name];
    }
    
    self.quiz.choice1 = [answerArray objectAtIndex:0];
    self.quiz.choice2 = [answerArray objectAtIndex:1];
    self.quiz.choice3 = [answerArray objectAtIndex:2];
    self.quiz.choice4 = [answerArray objectAtIndex:3];
    self.quiz.answer = _answerPosition;
    self.quiz.type = questionType;
    self.quiz.prompt = prompt;
    self.quiz.level = level;
    self.quiz.difficulty = state.difficulty;
    self.quiz.reverse = reverse;
    return YES;
}

-(NSMutableArray *)getStates:(NSString *)question withAnswer:(NSString *)answer forQuizType:(enum QuizType)quizType
{
    NSMutableSet *answerSet = [[NSMutableSet alloc] init];
    while([answerSet count] < 3)
    {
        State *state = [self.states objectAtIndex:arc4random() % _count];
        if(![state.name isEqualToString:answer])
        {
            BOOL skip = NO;
            switch(quizType)
            {
                case Capitals:
                    skip = [state.capital isEqualToString:question];
                    break;
                case Flags:
                case Maps:
                case QuizTypeNone:
                    break;
            }
            if(!skip)
            {
                [answerSet addObject:state.name];
            }
        }
    }
    NSMutableArray *answerArray = [[answerSet allObjects] mutableCopy];
    _answerPosition = arc4random() % 4;
    [answerArray insertObject:answer atIndex:_answerPosition];
    return answerArray;
}

-(NSMutableArray *)getCapitals:(NSString *)answer
{
    return [self getItems:^(State *s)
            {
                return s.capital;
            } withAnswer:answer];
}

-(NSMutableArray *)getFlags:(NSString *)answer
{
    return [self getItems:^(State *s)
            {
                return s.flag;
            } withAnswer:answer];
}

-(NSMutableArray *)getMaps:(NSString *)answer
{
    return [self getItems:^(State *s)
            {
                return s.map;
            } withAnswer:answer];
}

-(NSMutableArray *)getItems:(NSString *(^)(State *c))lambda withAnswer:(NSString *)answer
{
    NSMutableSet *answerSet = [[NSMutableSet alloc] init];
    while([answerSet count] < 3) {
        State *state = [self.states objectAtIndex:arc4random() % _count];
        NSString *choice = lambda(state);
        if(![choice isEqualToString:@""] && ![choice isEqualToString:answer])
            [answerSet addObject:choice];
    }
    NSMutableArray *answerArray = [[answerSet allObjects] mutableCopy];
    _answerPosition = arc4random() % 4;
    [answerArray insertObject:answer atIndex:_answerPosition];
    return answerArray;
}


@end

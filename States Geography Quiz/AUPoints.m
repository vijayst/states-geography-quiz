//
//  AUPoints.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 12/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

// difficulty - 1, 11 capitals, 12 flags, 16 maps.
const int kCapitalArray[3][4] = {{ 40, 20, 8, 4 },{ 12, 6, 3, 1 },{ 15, 8, 4, 1 }};
const int kFlagArray[3][4] = {{ 110, 55, 25, 10 },{ 24, 12, 6, 2 },{ 30, 16, 8, 2 }};
const int kMapArray[3][4] = {{ 80, 40, 20, 5 },{ 24, 12, 6, 2 },{ 30, 16, 8, 2 }};
const int kBonusPoints = 120;
const int kCapitalBonusPoints = 120;
const int kFlagBonusPoints = 200;
const int kMapBonusPoints = 80;
const int kMaxPoints = 880;
const int kTime1Points = 40;
const int kTime2Points = 20;
const int kTime3Points = 8;

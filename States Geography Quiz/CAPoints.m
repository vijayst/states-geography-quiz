//
//  CAPoints.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 10/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

// difficulty: 1 - 13 states.
const int kCapitalArray[3][4] = {{ 36, 20, 8, 4 },{ 12, 6, 3, 1 },{ 15, 8, 4, 1 }};
const int kFlagArray[3][4] = {{ 108, 60, 24, 12 },{ 24, 12, 6, 2 },{ 30, 16, 8, 2 }};
const int kMapArray[3][4] = {{ 108, 60, 24, 12 },{ 24, 12, 6, 2 },{ 30, 16, 8, 2 }};
const int kBonusPoints = 64;
const int kCapitalBonusPoints = 64;
const int kFlagBonusPoints = 128;
const int kMapBonusPoints = 128;
const int kMaxPoints = 936;
const int kTime1Points = 36;
const int kTime2Points = 20;
const int kTime3Points = 8;
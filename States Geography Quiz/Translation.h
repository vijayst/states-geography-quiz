//
//  Translation.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 09/11/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Translation : NSObject
-(NSString *)getTranslation:(NSString *)englishString;
@end

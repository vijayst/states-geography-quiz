//
//  Points.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Points.h"

@interface Points()
@end

@implementation Points

+(int)calculate:(enum QuizType)type attempt:(int)attempt time:(int)time difficulty:(int)difficulty
{
    int points = 0;
    switch(type)
    {
        case Capitals:
            points = [self getCapitalPoints:difficulty attempt:attempt];
            break;
        case Flags:
            points = [self getFlagPoints:difficulty attempt:attempt];
            break;
        case Maps:
            points = [self getMapPoints:difficulty attempt:attempt];
            break;
        case QuizTypeNone:
            break;
    }
    
    if(time > 12 && attempt==1)
        points += kTime1Points;
    else if(time>10 && attempt<=2)
        points += kTime2Points;
    else if(time>5 && attempt<=2)
        points += kTime3Points;
    
    return points;
}

+(int)getMaximumPoints:(enum GameType)gameType quizType:(enum QuizType)quizType
{
    if(gameType==GameTypeMulti)
        return (kMaxPoints + kBonusPoints) * 5;
    else
    {
        switch(quizType)
        {
            case Capitals:
                return kMaxPoints + kBonusPoints;
            case Flags:
            case Maps:
                return  2*(kMaxPoints + kBonusPoints);
            case QuizTypeNone:
                return 0;
        }
    }
    return 0;
}

+(int)getBonusPoints:(enum GameType)gameType quizType:(enum QuizType)quizType
{
    if(gameType==GameTypeMulti)
        return kBonusPoints*10;
    else
    {
        switch(quizType)
        {
            case Capitals:
                return kCapitalBonusPoints;
            case Flags:
                return kFlagBonusPoints;
            case Maps:
                return  kMapBonusPoints;
            case QuizTypeNone:
                return 0;
        }
    }
}

+(int)getCapitalPoints:(int)difficulty attempt:(int)attempt
{
    return kCapitalArray[difficulty-1][attempt-1];
}

+(int)getFlagPoints:(int)difficulty attempt:(int)attempt
{
    return kFlagArray[difficulty-1][attempt-1];
}

+(int)getMapPoints:(int)difficulty attempt:(int)attempt
{
    return kMapArray[difficulty-1][attempt-1];
}

@end

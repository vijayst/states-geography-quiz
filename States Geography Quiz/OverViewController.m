//
//  OverViewController.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 07/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Social/Social.h>
#import "OverViewController.h"
#import "Translation.h"
#import "Points.h"
#import <iAd/iAd.h>
@interface OverViewController ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *overLabel;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
- (IBAction)shareFacebook:(id)sender;
- (IBAction)shareTwitter:(id)sender;
- (IBAction)showMenu:(id)sender;
@end

@implementation OverViewController
{
    Translation *_translation;
    NSArray *_buttons;
    NSString *_shareText;
    NSURL *_shareUrl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    ADBannerView *adView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50, 320, 50)];
    [self.view addSubview:adView];
    // Do any additional setup after loading the view.
    [self initLabels];
    [self initButtons];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)initLabels
{
    _translation = [[Translation alloc] init];
    NSString *titleText = [_translation getTranslation:@"Game Over"];
    self.titleLabel.text = titleText;
    NSString *mainText = [_translation getTranslation:@"Main Menu"];
    [self.homeButton setTitle:mainText forState:UIControlStateNormal];
}

-(void)initButtons
{
    _buttons = @[self.facebookButton, self.twitterButton, self.homeButton];
    BOOL iPhone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    for(UIButton *button in _buttons)
    {
        button.layer.cornerRadius = iPhone ? 6.0F : 10.0F;
        button.layer.borderWidth = 1.0F;
        button.layer.borderColor = [[UIColor lightTextColor] CGColor];
    }
    NSString *facebookText = [_translation getTranslation:@"Share with Facebook"];
    [self.facebookButton setTitle:facebookText forState:UIControlStateNormal];
    NSString *twitterText = [_translation getTranslation:@"Share with Twitter"];
    [self.twitterButton setTitle:twitterText forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.facebookButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
    self.twitterButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
    
    self.game.score += [Points getBonusPoints:self.game.gameType quizType:self.game.quiz.type];
    int maxPoints = [Points getMaximumPoints:self.game.gameType quizType:self.game.quiz.type];
    
    NSString *gameOverText = [_translation getTranslation:@"Congrats! You scored %1$d out of %2$d points."];
    self.overLabel.text = [NSString stringWithFormat:gameOverText, self.game.score, maxPoints];
    self.overLabel.numberOfLines = 0;
    [self.overLabel sizeToFit];
    
    NSString *shareTextFormat = [_translation getTranslation:@"I scored %1$d out of %2$d points in %3$@."];
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    _shareText = [NSString stringWithFormat:shareTextFormat, self.game.score, maxPoints, productName];
    NSString *urlText = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"App Url"];
    _shareUrl = [NSURL URLWithString:urlText];
}

- (IBAction)showMenu:(id)sender {
    if(self.presentingViewController!=nil)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)shareFacebook:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:_shareText];
        [controller addURL:_shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (IBAction)shareTwitter:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:_shareText];
        [controller addURL:_shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}


@end

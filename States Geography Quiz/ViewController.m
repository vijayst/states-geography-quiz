//
//  ViewController.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 06/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "ViewController.h"
#import "GameViewController.h"
#import "OverViewController.h"
#import "AudioHelper.h"
#import "Game.h"
#import <iAd/iAd.h>
@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *fullgameButton;
@property (strong, nonatomic) IBOutlet UIButton *capitalButton;
@property (strong, nonatomic) IBOutlet UIButton *flagButton;
@property (strong, nonatomic) IBOutlet UIButton *mapButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spacing1Constraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spacing2Constraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spacing3Constraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topspacingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightspacingConstraint;

- (IBAction)fullGameClick:(id)sender;
- (IBAction)capitalGameClick:(id)sender;
- (IBAction)flagGameClick:(id)sender;
- (IBAction)mapGameClick:(id)sender;

@end

@implementation ViewController
{
    AudioHelper *_audioHelper;
    UIDynamicAnimator *_animator;
    NSArray *_buttons;
    
    GameViewController *_gameController;
    Game *_fullGame;
    Game *_capitalGame;
    Game *_flagGame;
    Game *_currencyGame;
    Game *_continentGame;
    Game *_mapGame;
    Game *_currentGame;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    ADBannerView *adView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50, 320, 50)];
    [self.view addSubview:adView];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    _audioHelper = [[AudioHelper alloc] init];
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        // update constraints based on size of iPhone.
        [self updateConstraints];
    }
    
    _buttons = @[self.fullgameButton, self.capitalButton, self.flagButton, self.mapButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.view.hidden = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    if(!self.silent)
    {
        [_audioHelper playAppLoadSound];
        [self animate];
    }
    self.view.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateConstraints
{
    CGFloat h = self.view.bounds.size.height;
    CGFloat spacing = 46.0F;
    CGFloat topspacing = 55.0F;
    if(h==480)
    {
        spacing = 26.7F;
        topspacing = 40.0F;
    }
    if(h==667)
    {
        spacing = 59.0F;
        topspacing = 85.0F;
    }
    if(h==736)
    {
        spacing = 72.0F;
        topspacing = 100.0F;
    }
 
    self.topspacingConstraint.constant = topspacing;
    self.spacing1Constraint.constant = spacing;
    self.spacing2Constraint.constant = spacing;
    self.spacing3Constraint.constant = spacing;
}

-(void)animate
{
    CGRect bounds = [self.view bounds];
    CGFloat fullWidth = bounds.size.width;
    CGFloat right = fullWidth - self.rightspacingConstraint.constant;
    CGFloat top = bounds.origin.y;
    CGFloat bottom = bounds.origin.y + bounds.size.height;
    
    CGPoint topRight = CGPointMake(right, top);
    CGPoint bottomRight = CGPointMake(right, bottom);
    
    int index = 1;
    for(UIButton *button in _buttons)
    {
        CGRect buttonFrame = button.frame;
        buttonFrame.origin.x = - buttonFrame.size.width - 200.0 * index;
        button.frame = buttonFrame;
        index++;
    }
    
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    // Add gravity behavior - horizontal direction
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:_buttons];
    gravityBehavior.gravityDirection = CGVectorMake(1.0, 0.0);
    [_animator addBehavior:gravityBehavior];
    
    // Add collision behavior
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:_buttons];
    collisionBehavior.collisionMode = UICollisionBehaviorModeBoundaries;
    
    [collisionBehavior addBoundaryWithIdentifier:@"stop"
                                       fromPoint:topRight
                                         toPoint:bottomRight];
    [_animator addBehavior:collisionBehavior];
    
    // Add elasticity
    UIDynamicItemBehavior *dynamicBehavior = [[UIDynamicItemBehavior alloc] initWithItems:_buttons];
    dynamicBehavior.elasticity = 0.25;
    [_animator addBehavior:dynamicBehavior];
}

- (IBAction)fullGameClick:(id)sender {
    if(_fullGame==nil || _fullGame.over)
        _fullGame = [Game createGame:GameTypeMulti andType:QuizTypeNone];
    [self presentGameController:_fullGame];
}

- (IBAction)capitalGameClick:(id)sender {
    if(_capitalGame==nil || _capitalGame.over)
        _capitalGame = [Game createGame:GameTypeSingle andType:Capitals];
    [self presentGameController:_capitalGame];
}

- (IBAction)flagGameClick:(id)sender {
    if(_flagGame==nil || _flagGame.over)
        _flagGame = [Game createGame:GameTypeSingle andType:Flags];
    [self presentGameController:_flagGame];
}

- (IBAction)mapGameClick:(id)sender {
    if(_mapGame==nil || _mapGame.over)
        _mapGame = [Game createGame:GameTypeSingle andType:Maps];
    [self presentGameController:_mapGame];
}


-(void)presentGameController:(Game *)game
{
    _currentGame = game;
    if(_gameController==nil) {
        _gameController = [self.storyboard instantiateViewControllerWithIdentifier:@"gameId"];
    }
    _gameController.game = game;
    [self presentViewController:_gameController
                       animated:NO
                     completion:nil];
}

-(void)showGameOver
{
    OverViewController *overController = [self.storyboard instantiateViewControllerWithIdentifier:@"overId"];
    overController.game = _currentGame;
    [self presentViewController:overController
                       animated:YES
                     completion:nil];
}

@end

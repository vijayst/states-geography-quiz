//
//  AnimationHelper.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 21/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AnimationCompletedDelegate<NSObject>
@optional
-(void)animationCompleted;
@end

@interface AnimationHelper : NSObject
-(void) zoom:(UIView *)view;
-(void) fade:(UIView *)view;
@property (assign, nonatomic) id<AnimationCompletedDelegate> delegate;
@end

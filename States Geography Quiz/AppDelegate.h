//
//  AppDelegate.h
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 06/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


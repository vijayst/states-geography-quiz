//
//  USPoints.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 09/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

// difficulty: 39, 7, 5
const int kCapitalArray[3][4] = {{ 9, 5, 2, 1 },{ 12, 6, 3, 1 },{ 15, 8, 4, 1 }};
const int kFlagArray[3][4] = {{ 25, 15, 5, 2 },{ 40, 20, 10, 2 },{ 50, 25, 12, 3 }};
const int kMapArray[3][4] = {{ 25, 15, 5, 2 },{ 40, 20, 10, 2 },{ 50, 25, 12, 3 }};
const int kBonusPoints = 31;
const int kCapitalBonusPoints = 31;
const int kFlagBonusPoints = 36;
const int kMapBonusPoints = 36;
const int kMaxPoints = 969;
const int kTime1Points = 9;
const int kTime2Points = 5;
const int kTime3Points = 2;

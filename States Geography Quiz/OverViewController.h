//
//  OverViewController.h
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 07/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface OverViewController : UIViewController
@property (strong, nonatomic) Game* game;
@end

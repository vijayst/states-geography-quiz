//
//  AusState.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 12/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "AusState.h"
#import "DbHelper.h"

@implementation AusState

+(NSArray *)getStates
{
    DbHelper *helper = [[DbHelper alloc] init];
    [helper open];
    sqlite3_stmt *statement;
    [helper executeQuery:@"SELECT State, Code, Capital, has_flag FROM AustraliaStatesCapitals" statement:&statement];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    // int i = 0;
    while (sqlite3_step(statement) == SQLITE_ROW) {
        // if(i++==4) break;
        State *state = [[State alloc] init];
        state.name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
        char *capital = (char *)sqlite3_column_text(statement, 2);
        state.capital = @"";
        if(capital!=nil)
            state.capital = [[NSString alloc] initWithUTF8String:capital];
        state.iso = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
        state.flag = state.map = state.iso;
        BOOL hasFlag = sqlite3_column_int(statement, 3);
        if(!hasFlag)
            state.flag = @"";
        state.difficulty = 1;
        [array addObject:state];
    }
    sqlite3_finalize(statement);
    [helper close];
    return array;
}

@end

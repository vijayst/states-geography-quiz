//
//  main.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 06/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

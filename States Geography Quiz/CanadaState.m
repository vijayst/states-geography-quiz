//
//  CAState.m
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 10/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "CanadaState.h"
#import "DbHelper.h"

@implementation CanadaState

+(NSArray *)getStates
{
    DbHelper *helper = [[DbHelper alloc] init];
    [helper open];
    sqlite3_stmt *statement;
    [helper executeQuery:@"SELECT BGNlc_name, ISO31662A2, Lc_capital from statesOfCountries" statement:&statement];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    // int i = 0;
    while (sqlite3_step(statement) == SQLITE_ROW) {
        // if(i++==4) break;
        State *state = [[State alloc] init];
        state.name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
        state.capital = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
        state.iso = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
        state.flag = state.map = state.name;
        state.difficulty = 1;
        [array addObject:state];
    }
    sqlite3_finalize(statement);
    [helper close];
    return array;
}

@end

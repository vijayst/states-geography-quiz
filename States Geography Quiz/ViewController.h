//
//  ViewController.h
//  States Geography Quiz
//
//  Created by Vijay Thirugnanam on 06/12/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (assign, nonatomic) BOOL silent;
-(void)showGameOver;
@end

